# Modificado de https://markcaudill.me/blog/2012/07/dynamic-route53-dns-updating-with-python/
from area53 import route53
from boto.route53.exception import DNSServerError
import requests
import sys
from datetime import datetime

domain = 'vixtec.com.br'
subdomain = 'cliente'

def get_public_ip():
    r = requests.get('https://v4.ident.me')
    return r.text.rstrip()

fqdn = '%s.%s' % (subdomain, domain)
zone = route53.get_zone(domain)
arec = zone.get_a(fqdn)
New_IPv4 = get_public_ip()

if arec:
        Old_IPv4 = arec.resource_records[0]

        if Old_IPv4 == New_IPv4:
                print ('%s atualizado. (%s)' % (fqdn, New_IPv4))
                sys.exit(0)

        print ('Atualizando %s: %s -> %s' % (fqdn, Old_IPv4, New_IPv4))

        try:
                zone.update_a(fqdn, New_IPv4, 900)

        except DNSServerError:
                # Isso pode acontecer se o registro ainda não existir.
                # Vamos tentar adicionar um, caso seja o caso.
                zone.add_a(fqdn, New_IPv4, 900)
else:
        zone.add_a(fqdn, New_IPv4, 900)