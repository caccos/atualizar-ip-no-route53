# ATUALIZAR IP EM UM REGISTRO A DO ROUTE 53

Este projeto tem o intuito de atualizar seu IPv4 Público em um registro A do Route 53, criando assim um serviço de DDNS que você poderá usar em seus clientes. Basta colocar no Agendador de Tarefas para atualizar a cada 5 minutos.

# Como usar

  - Instale o AWS CLI e configure com um usuário que tenha permissão no Route 53;
  - Instalar o Python e as dependências Area53, Boto e Requests da seguinte forma:
     - CD \Caminho_da_pasta_Area53
     - C:\Caminho do seu Python\python.exe setup.py install
     - CD \Caminho_da_pasta_Boto
     - C:\Caminho do seu Python\python.exe setup.py install
     - CD \Caminho_da_pasta_Requests
     - C:\Caminho do seu Python\python.exe setup.py install

 - Edite o script.py e altere as variáveis domain e subdomain;
 - Crie um arquivo .bat com o comando C:\Caminho do seu Python\python.exe C:\Caminho_do_Script.py
 - Crie um agendamento no Agendador de tarefas do Windows, para executar o .bat de 5 em 5 minutos.
 
  
